export default () => ({
    isLoading: true,
    entries: [
        {
            id: new Date().getTime(), //12135431
            date: new Date().toDateString(), // sat 18, diciembre
            text: 'Voluptate officia est officia reprehenderit proident ullamco. Commodo laboris cillum officia amet in est aute incididunt consectetur enim aliquip. Occaecat in proident esse duis culpa pariatur incididunt tempor culpa nisi eiusmod qui minim veniam. Nisi fugiat occaecat est nostrud consectetur occaecat. Pariatur deserunt cupidatat labore dolore culpa quis non amet irure. Ad do qui ea esse. Nostrud in amet in fugiat occaecat qui irure fugiat duis amet.',
            picture: null, // hhtps://
        },
        {
            id: new Date().getTime() + 1000, //12135431
            date: new Date().toDateString(), // sat 18, diciembre
            text: 'Officia dolore excepteur eiusmod Lorem mollit Lorem aliqua. Voluptate mollit exercitation irure fugiat mollit id ad qui nisi. Laborum enim non nisi voluptate sunt in Lorem do ad magna. Ipsum officia elit Lorem velit eiusmod in laborum.',
            picture: null, // hhtps://
        },
        {
            id: new Date().getTime() + 2000, //12135431
            date: new Date().toDateString(), // sat 18, diciembre
            text: 'Nostrud nulla do consectetur cillum incididunt sit cupidatat et excepteur deserunt laborum laboris qui cillum. Labore deserunt dolore ullamco enim occaecat elit quis laboris ex aliqua. Sit mollit enim incididunt excepteur fugiat. Elit commodo commodo cupidatat anim nisi dolor est elit. Cupidatat qui do qui magna magna dolor fugiat cillum adipisicing dolor quis. Veniam est duis ad voluptate ullamco deserunt sit. Exercitation consectetur nulla Lorem magna aliqua aliqua occaecat exercitation.',
            picture: null, // hhtps://
        },
    ]
})